using System;
using Member.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;


namespace Ioliz
{
  public class Program
  {

    public static void Main(string[] args)
    {
      var host = CreateHostBuilder(args).Build();
    
      var services = host.Services;
      try
      {

        AppInstance.Initialize(services);
        DbInitializer.Initialize(services).Wait();
      }
      catch (Exception ex)
      {
        var logger = services.GetRequiredService<ILogger<Program>>();
        logger.LogError(ex, "");
      }


      host.Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
       Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
              webBuilder.UseUrls("http://*:7800")
                .UseStartup<Startup>();
            });

  }

}
